# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* REST API

POST /number - генерация случайного числа

GET /number/{id} - получение результата генерации

### How do I get set up? ###

* Dependencies
>composer update
***
(в composer.json прописано, но на всякий случай - phpunit должен быть версии 5.7 (не 6.0))

* Database configuration
>php artisan migrate
***
>php artisan db:seed --class=NumbersTableSeeder

* How to run tests
>phpunit