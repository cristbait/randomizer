<?php

namespace App\Http\Controllers;

use App\Number;
use Illuminate\Http\Request;

class NumberController extends Controller
{

    public function retrieve($id)
    {
        $number = Number::find($id);
        if ($number) {
            return response()->json($number, 200);
        }
        else {
            abort(404);
        }
    }

    public function generate()
    {
        $newNumber = new Number;
        $newNumber->number = rand();
        if ($newNumber->save()){
            return response()->json($newNumber, 201);
        }
        else {
            abort(404);
        }

    }



}
