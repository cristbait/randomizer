<?php

use App\Number;
use Illuminate\Database\Seeder;

class NumbersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newNumber = new Number;
        $newNumber->number = rand();
        $newNumber->save();
    }
}
