<?php

namespace Tests\Feature;

use App\Number;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResponseTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        DB::beginTransaction();
        Artisan::call('migrate');
        Artisan::call('db:seed', ['--class' => 'NumbersTableSeeder']);
    }

    public function tearDown()
    {
        DB::rollBack();
    }

    /**
     * A generation test.
     *
     * @return void
     */
    public function testGenerate()
    {
        $response = $this->post('/api/number');
        $response
            ->assertStatus(201)
            ->assertJson([]);
    }

    /**
     * A retrieval test.
     *
     * @return void
     */
    public function testRetrieve()
    {
        $number = Number::all()->first();
        $response = $this->get('/api/number/' . $number->id);
        $response
            ->assertStatus(200)
            ->assertJson([
                'number' => $number->number,
            ]);
    }

}
